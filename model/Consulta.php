<?php


class Consulta {
    private $id;
    private $historico_clinico;
    private $foto;
    private $video;

    /**
     * Consulta constructor.
     * @param $historico_clinico
     * @param $foto
     * @param $video
     */
    public function __construct($historico_clinico, $foto, $video)
    {
        $this->historico_clinico = $historico_clinico;
        $this->foto = $foto;
        $this->video = $video;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getHistoricoClinico()
    {
        return $this->historico_clinico;
    }

    /**
     * @param mixed $historico_clinico
     */
    public function setHistoricoClinico($historico_clinico)
    {
        $this->historico_clinico = $historico_clinico;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param mixed $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    /**
     * @return mixed
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * @param mixed $video
     */
    public function setVideo($video)
    {
        $this->video = $video;
    }
}
