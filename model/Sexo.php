<?php

class Sexo {
    private $id;
    private $descricao;
    private $sigla;

    /**
     * Sexo constructor.
     * @param $id
     * @param $descricao
     * @param $sigla
     */
    public function __construct($descricao, $sigla)
    {
        $this->descricao = $descricao;
        $this->sigla = $sigla;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getSigla()
    {
        return $this->sigla;
    }

    /**
     * @param mixed $sigla
     */
    public function setSigla($sigla)
    {
        $this->sigla = $sigla;
    }
}
