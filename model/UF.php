<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 10/7/2017
 * Time: 3:40 PM
 */

class UF
{
    private $id;
    private $descricao;
    private $sigla;

    private $pais;

    /**
     * UF constructor.
     * @param $descricao
     * @param $sigla
     * @param $pais
     */
    public function __construct($descricao, $sigla, $pais)
    {
        $this->descricao = $descricao;
        $this->sigla = $sigla;
        $this->pais = $pais;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getSigla()
    {
        return $this->sigla;
    }

    /**
     * @param mixed $sigla
     */
    public function setSigla($sigla)
    {
        $this->sigla = $sigla;
    }

    /**
     * @return mixed
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * @param mixed $pais
     */
    public function setPais($pais)
    {
        $this->pais = $pais;
    }
}