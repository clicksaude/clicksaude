<?php

class Pais {
    private $id;
    private $nome;
    private $nacionalidade;

    /**
     * Pais constructor.
     * @param $nome
     * @param $nacionalidade
     */
    public function __construct($nome, $nacionalidade)
    {
        $this->nome = $nome;
        $this->nacionalidade = $nacionalidade;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getNacionalidade()
    {
        return $this->nacionalidade;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @param mixed $nacionalidade
     */
    public function setNacionalidade($nacionalidade)
    {
        $this->nacionalidade = $nacionalidade;
    }


}
