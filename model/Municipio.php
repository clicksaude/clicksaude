<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 10/7/2017
 * Time: 3:40 PM
 */

class Municipio
{
    private $id;
    private $nome;

    private $uf;

    /**
     * Municipio constructor.
     * @param $nome
     * @param $uf
     */
    public function __construct($nome, $uf)
    {
        $this->nome = $nome;
        $this->uf = $uf;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
    }
}