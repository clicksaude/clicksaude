<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 10/13/2017
 * Time: 8:10 PM
 */

class Usuario
{
    private $id;
    private $username;
    private $senha;

    private $pessoa;

    public function __construct($username, $senha, $pessoa) {
        $this->username = $username;
        $this->senha = $senha;
        $this->pessoa = $pessoa;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return mixed
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * @param mixed $pessoa
     */
    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
    }
}