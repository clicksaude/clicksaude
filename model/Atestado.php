<?php


class Atestado {
    private $id;
    private $tipo;
    private $dias_atestado;
    private $data_consulta;
    function __construct($tipo, $dias_atestado, $data_consulta) {
        $this->tipo = $tipo;
        $this->dias_atestado = $dias_atestado;
        $this->data_consulta = $data_consulta;
    }
    function getId() {
        return $this->id;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getDias_atestado() {
        return $this->dias_atestado;
    }

    function getData_consulta() {
        return $this->data_consulta;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setDias_atestado($dias_atestado) {
        $this->dias_atestado = $dias_atestado;
    }

    function setData_consulta($data_consulta) {
        $this->data_consulta = $data_consulta;
    }


}
