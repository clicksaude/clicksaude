<?php

require_once 'model/Endereco.php';
require_once 'model/Usuario.php';

class Pessoa {
    private $id;
    private $nome;
    private $data_nascimento;
    private $rg;
    private $cpf;

    private $sexo;

   // private $profissional;

    private $endereco;

    private $usuario;

    private $telefonePrincipal;
    private $telefoneSecundario;

    /**
     * Pessoa constructor.
     * @param $nome
     * @param $data_nascimento
     * @param $rg
     * @param $cpf
     * @param $sexo
     * @param $endereco
     */
    public function __construct($nome, $data_nascimento, $rg, $cpf, $senha, $sexo, $rua, $numero, $cep, $telefonePrincipal = null, $telefoneSecundario = null)
    {
        $this->nome = $nome;
        $this->data_nascimento = $data_nascimento;
        $this->rg = $rg;
        $this->cpf = $cpf;
        $this->usuario = new Usuario($nome, $senha, $this);
        $this->sexo = $sexo;
        $this->endereco = new Endereco($rua, $numero, $cep, $this);
    //    $this->profissional = new Profissional($tipo, $this);
        $this->telefonePrincipal = $telefonePrincipal;
        $this->telefoneSecundario = $telefoneSecundario;
    }


  //  public function setProfissional($profissional)
 //   {
    //    $this->profissional = $profissional;
 //   }
 //   public function getProfissional()
  //  {
     //   return $this->profissional;
  //  }


    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    public function getSexo()
    {
        return $this->sexo;
    }

    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    public function getEndereco()
    {
        return $this->endereco;
    }

    public function setEndereco($rua, $numero, $cep)
    {
        $this->endereco = new Endereco($rua, $numero, $cep, $this);
    }

    /**
     * @return mixed
     */
    public function getDataNascimento()
    {
        return $this->data_nascimento;
    }

    /**
     * @param mixed $data_nascimento
     */
    public function setDataNascimento($data_nascimento)
    {
        $this->data_nascimento = $data_nascimento;
    }

    /**
     * @return mixed
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param mixed $rg
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param Usuario $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getTelefonePrincipal()
    {
        return $this->telefonePrincipal;
    }

    /**
     * @param mixed $telefonePrincipal
     */
    public function setTelefonePrincipal($telefonePrincipal)
    {
        $this->telefonePrincipal = $telefonePrincipal;
    }

    /**
     * @return mixed
     */
    public function getTelefoneSecundario()
    {
        return $this->telefoneSecundario;
    }

    /**
     * @param mixed $telefoneSecundario
     */
    public function setTelefoneSecundario($telefoneSecundario)
    {
        $this->telefoneSecundario = $telefoneSecundario;
    }
}
