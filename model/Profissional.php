<?php


class Profissional {
    private $id;
    private $tipo;
    private $pessoa;

    function __construct($tipo, $pessoa) {
        $this->tipo = $tipo;
        $this->pessoa = $pessoa;
    }
    function getId() {
        return $this->id;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getPessoa() {
        return $this->pessoa;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setPessoa($pessoa) {
        $this->pessoa = $pessoa;
    }


}
