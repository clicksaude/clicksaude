<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Paciente
 *
 * @author Murilo
 */
class Paciente {
     private $id;
     private $convenios;
     private $paciente_antigo_especial;
     private $tipo_indicacao;
     private $altura;
     private $peso;
     private $pessoa;

     function __construct($convenios, $paciente_antigo_especial, $tipo_indicacao, $altura, $peso, $pessoa) {
         $this->convenios = $convenios;
         $this->paciente_antigo_especial = $paciente_antigo_especial;
         $this->tipo_indicacao = $tipo_indicacao;
         $this->altura = $altura;
         $this->peso = $peso;
         $this->pessoa = $pessoa;
     }
     function getId() {
         return $this->id;
     }

     function getConvenios() {
         return $this->convenios;
     }

     function getPaciente_antigo_especial() {
         return $this->paciente_antigo_especial;
     }

     function getTipo_indicacao() {
         return $this->tipo_indicacao;
     }

     function getAltura() {
         return $this->altura;
     }

     function getPeso() {
         return $this->peso;
     }

     function getPessoa() {
         return $this->pessoa;
     }

     function setId($id) {
         $this->id = $id;
     }

     function setConvenios($convenios) {
         $this->convenios = $convenios;
     }

     function setPaciente_antigo_especial($paciente_antigo_especial) {
         $this->paciente_antigo_especial = $paciente_antigo_especial;
     }

     function setTipo_indicacao($tipo_indicacao) {
         $this->tipo_indicacao = $tipo_indicacao;
     }

     function setAltura($altura) {
         $this->altura = $altura;
     }

     function setPeso($peso) {
         $this->peso = $peso;
     }

     function setPessoa($pessoa) {
         $this->pessoa = $pessoa;
     }


}
