<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 10/11/2017
 * Time: 2:31 PM
 */

class Endereco
{
    private $id;
    private $numero;
    private $rua;
    private $cep;

    /**
     * Endereco constructor.
     * @param $numero
     * @param $rua
     * @param $cep
     */
    public function __construct($numero, $rua, $cep)
    {
        $this->numero = $numero;
        $this->rua = $rua;
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getRua()
    {
        return $this->rua;
    }

    /**
     * @param mixed $rua
     */
    public function setRua($rua)
    {
        $this->rua = $rua;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }


}