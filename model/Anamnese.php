<?php


class Anamnese {


    private $id;
    private $atividade_fisica;
    private $medicamentos_em_uso;
    private $queixas;
    private $fraturas;
    private $cirurgia;
    private $arquivoPdf_cadastrosAntigos;
    function __construct($atividade_fisica, $medicamentos_em_uso, $queixas, $fraturas, $cirurgia, $arquivoPdf_cadastrosAntigos) {
        $this->atividade_fisica = $atividade_fisica;
        $this->medicamentos_em_uso = $medicamentos_em_uso;
        $this->queixas = $queixas;
        $this->fraturas = $fraturas;
        $this->cirurgia = $cirurgia;
        $this->arquivoPdf_cadastrosAntigos = $arquivoPdf_cadastrosAntigos;
        
    }
    function getId() {
        return $this->id;
    }

    function getAtividade_fisica() {
        return $this->atividade_fisica;
    }

    function getMedicamentos_em_uso() {
        return $this->medicamentos_em_uso;
    }

    function getQueixas() {
        return $this->queixas;
    }

    function getFraturas() {
        return $this->fraturas;
    }

    function getCirurgia() {
        return $this->cirurgia;
    }

    function getArquivoPdf_cadastrosAntigos() {
        return $this->arquivoPdf_cadastrosAntigos;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setAtividade_fisica($atividade_fisica) {
        $this->atividade_fisica = $atividade_fisica;
    }

    function setMedicamentos_em_uso($medicamentos_em_uso) {
        $this->medicamentos_em_uso = $medicamentos_em_uso;
    }

    function setQueixas($queixas) {
        $this->queixas = $queixas;
    }

    function setFraturas($fraturas) {
        $this->fraturas = $fraturas;
    }

    function setCirurgia($cirurgia) {
        $this->cirurgia = $cirurgia;
    }

    function setArquivoPdf_cadastrosAntigos($arquivoPdf_cadastrosAntigos) {
        $this->arquivoPdf_cadastrosAntigos = $arquivoPdf_cadastrosAntigos;
    }


}
