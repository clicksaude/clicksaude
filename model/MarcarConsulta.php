<?php


class MarcarConsulta {
    private $id;
    private $data_consulta;
    private $paciente;
    private $profissional;

    /**
     * MarcarConsulta constructor.
     * @param $data_consulta
     * @param $paciente
     * @param $profissional
     */
    public function __construct($data_consulta, $paciente, $profissional)
    {
        $this->data_consulta = $data_consulta;
        $this->paciente = $paciente;
        $this->profissional = $profissional;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDataConsulta()
    {
        return $this->data_consulta;
    }

    /**
     * @param mixed $data_consulta
     */
    public function setDataConsulta($data_consulta)
    {
        $this->data_consulta = $data_consulta;
    }

    /**
     * @return mixed
     */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * @param mixed $paciente
     */
    public function setPaciente($paciente)
    {
        $this->paciente = $paciente;
    }

    /**
     * @return mixed
     */
    public function getProfissional()
    {
        return $this->profissional;
    }

    /**
     * @param mixed $profissional
     */
    public function setProfissional($profissional)
    {
        $this->profissional = $profissional;
    }
}
