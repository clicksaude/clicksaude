<?php
include_once 'controller/UsuarioController.php';
class MenuController
{

    private $usuarioController;


    public function __construct()
    {
        $this->usuarioController = new UsuarioController();
    }

    public function exibirMenu()
    {
        if ($this->usuarioController->usuarioEstaLogado()) {
            include 'view/cabecalho.php';
        } else {
            $this->usuarioController->form_login();
        }
    }

    public function exibirConsultas()
    {

        if ($this->usuarioController->usuarioEstaLogado()) {
            include_once 'view/bemvindo.php';
        } else {
            $this->usuarioController->form_login();
        }

    }
}
