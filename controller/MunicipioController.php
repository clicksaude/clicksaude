<?php

include_once './dao/MunicipioDAO.php';
include_once './dao/UFDAO.php';

include_once './model/Municipio.php';

class MunicipioController {

    private $municipioDAO;
    private $UFDAO;

    public function __construct() {
        $this->municipioDAO = new MunicipioDAO();
        $this->UFDAO = new UFDAO();
    }

    public function form_insercao() {
        $acao = 'insercao';
        $municipio = null;
        $rotuloBotao = "Inserir";
        $ufs = $this->UFDAO->listar();
        include_once 'view/municipio/form.php';
        $this->listar();
    }

    public function insercao() {
        $municipio = new municipio($_POST['nome'], $_POST['unidade_federativa_id']);
        $this->municipioDAO->inserir($municipio);
        $this->form_insercao();
    }

    public function listar() {
        $municipios = $this->municipioDAO->listar();
        include_once 'view/municipio/listar.php';
    }

    public function alteracao() {
        $municipio = new municipio($_POST['nome'], $_POST['unidade_federativa_id']);

        $municipio->setId($_POST['id']);

        $this->municipioDAO->alterar($municipio);
        $this->form_insercao();
    }

    public function form_alteracao() {
        $acao = 'alteracao';
        $municipio = $this->municipioDAO->buscar($_GET['id']);
        $rotuloBotao = "Alterar";
        $ufs = $this->UFDAO->listar();
        include_once 'view/municipio/form.php';
    }

    public function exclusao() {
        $this->municipioDAO->excluir($_GET['id']);
        $this->form_insercao();
    }
}
