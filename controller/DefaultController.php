<?php
include_once 'controller/MenuController.php';

include_once './controller/PessoaController.php';
include_once './controller/UsuarioController.php';

class DefaultController {
    
    private $menu;
    
    public function __construct()
    {
        $this->menu = new MenuController();
        $this->menu->exibirMenu();
        $this->menu->exibirConsultas();

    }
}
