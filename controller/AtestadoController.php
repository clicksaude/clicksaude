<?php

include_once 'dao/AtestadoDAO.php';
include_once 'model/Atestado.php';

class AtestadoController {

    private $atestadoDAO;

    
    public function __construct() {
        $this->atestadoDAO = new AtestadoDAO();
       // (new UsuarioController())->verificaUsuario();
    }
    
    public function form_insercao() {
        $acao = 'insercao';
        $atestado = null;
        $rotuloBotao = "Inserir";
        include_once 'view/atestado/form.php';
        $this->listar();
    }
    
    public function form_alteracao() {
        $acao = 'alteracao';
        $rotuloBotao = "Alterar";
        $atestado = $this->atestadoDAO->buscar($_GET['id']);
        include_once 'view/atestado/form.php';
    }
    
    public function exclusao() {
        $this->atestadoDAO->excluir($_GET['id']);
        $this->form_insercao();
       
    }
    
    public function insercao() {
        $atestado = new Atestado($_POST['tipo'], $_POST['dias_atestado'],$_POST['data_consulta']);
        $this->atestadoDAO->inserir($atestado);
        $this->form_insercao();     
        
    }
    
    public function alteracao() {
        $atestado = new Atestado($_POST['tipo'], $_POST['dias_atestado'],$_POST['data_consulta']);
        $atestado ->setId($_POST['id']);
        $this->atestadoDAO->alterar($atestado);
        $this->form_insercao();        
    }
    
    public function listar() {
        $atestados = $this->atestadoDAO->listar();
        include_once 'view/atestado/listar.php';
        
    }

}
