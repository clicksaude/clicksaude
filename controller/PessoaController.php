<?php

include_once 'dao/PessoaDAO.php';
include_once 'model/Pessoa.php';
include_once 'model/Sexo.php';

class PessoaController {
   
    private $pessoaDAO;
    private $sexoDAO;

    public function __construct() {
        $this->pessoaDAO = new PessoaDAO();
        $this->sexoDAO = new SexoDAO();
        //(new UsuarioController())->validaUsuario();
    }
    
    public function form_insercao() {
        $acao = 'insercao';
        $pessoa = null;
        $rotuloBotao = "Inserir";
        $sexos = $this->sexoDAO->listar();
        include_once 'view/pessoa/form.php';
        //$this->listar();
    }
    
    public function form_alteracao() {
        $acao = 'alteracao';
        $rotuloBotao = "Alterar";
        $pessoa = $this->pessoaDAO->buscar($_GET['id']);
        $sexos = $this->sexoDAO->listar();
        include_once 'view/pessoa/form.php';
    }
    
    public function exclusao() {
        $this->pessoaDAO->excluir($_GET['id']);
        $this->form_insercao();
       
    }
    
    public function insercao() {
        $pessoa = new Pessoa($_POST['nome'], $_POST['data_nasc'], $_POST['rg'], $_POST['cpf'], $_POST['senha'],
                             $_POST['sexo_id'], $_POST['rua'], $_POST['numero'], $_POST['cep'], $_POST['telefoneprincipal'],
                             $_POST['telefonesecundario']);
        $this->pessoaDAO->inserir($pessoa);
        $this->form_insercao();     
        
    }
    
    public function alteracao() {
        $pessoa = new Pessoa($_POST['nome'], $_POST['data_nasc'], $_POST['rg'], $_POST['cpf'], $_POST['senha'],
                             $_POST['sexo_id'], $_POST['rua'], $_POST['numero'], $_POST['cep'], $_POST['telefoneprincipal'],
                             $_POST['telefonesecundario']);
        $pessoa->setId($_POST['id']);
        $this->pessoaDAO->alterar($pessoa);
        $this->form_insercao();        
    }
    
    public function listar() {
        $pessoas = $this->pessoaDAO->listar();
        include_once 'view/pessoa/listar.php';
    }
    
}