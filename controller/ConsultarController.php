<?php

include_once 'dao/ConsultarDAO.php';

include_once 'model/Consulta.php';

class ConsultarController {
   
    private $consultarDAO;

    public function __construct() {
        $this->consultarDAO = new ConsultarDAO();
        //(new UsuarioController())->validaUsuario();
    }

    public function form_alteracao() {
        $acao = 'alteracao';
        $consulta = $this->consultarDAO->buscar($_GET['id']);
        $rotuloBotao = "Finalizar";
        include_once 'view/consulta/form.php';
    }

    public function alteracao() {
        $consulta = new Consulta($_POST['historico_clinico'], $_POST['foto'], $_POST['video']);
        $consulta->setId($_POST['id']);
        $this->consultarDAO->alterar($consulta);
        header('Location: ../clicksaude/index.php');
    }
}