<?php

include_once './dao/PaisDAO.php';
include_once './model/Pais.php';

class PaisController
{
    private $paisDAO;

    public function __construct() {
        $this->paisDAO = new PaisDAO();
    }

    public function form_insercao() {
        $acao = 'insercao';
        $pais = null;
        $rotuloBotao = "Inserir";
        include_once 'view/pais/form.php';
        $this->listar();
    }

    public function form_alteracao() {
        $acao = 'alteracao';
        $rotuloBotao = "Alterar";
        $pais = $this->paisDAO->buscar($_GET['id']);
        include_once 'view/pais/form.php';
    }

    public function exclusao() {
        $this->paisDAO->excluir($_GET['id']);
        $this->form_insercao();

    }

    public function insercao() {
        $pais = new Pais($_POST['nome'],$_POST['nacionalidade']);
        $this->paisDAO->inserir($pais);
        $this->form_insercao();
    }

    public function alteracao() {
        $pais = new Pais($_POST['nome'],$_POST['nacionalidade']);
        $pais->setId($_POST['id']);
        $this->paisDAO->alterar($pais);
        $this->form_insercao();
    }

    public function listar() {
        $paises = $this->paisDAO->listar();
        include_once 'view/pais/listar.php';

    }

}
