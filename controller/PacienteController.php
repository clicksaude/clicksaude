<?php

include_once 'model/Pessoa.php';
include_once 'model/Sexo.php';
include_once './dao/PacienteDAO.php';
include_once './dao/PessoaDAO.php';
include_once './model/Paciente.php';


class PacienteController
{

    private $pessoaDAO;
    private $sexoDAO;
    private $pacienteDAO;

    public function __construct()
    {
        $this->pessoaDAO = new PessoaDAO();
        $this->pacienteDAO = new PacienteDAO();
        $this->sexoDAO = new SexoDAO();
        //(new UsuarioController())->validaUsuario();
    }

    public function form_insercao()
    {
        $acao = 'insercao';
        $paciente = null;
        $pessoa = null;
        $rotuloBotao = "Inserir Paciente";
        $sexos = $this->sexoDAO->listar();
        include_once 'view/paciente/form.php';
        //$this->listar();
    }


    public function insercao()
    {
        $pessoa = new Pessoa($_POST['nome'], $_POST['data_nasc'], $_POST['rg'], $_POST['cpf'], $_POST['senha'],
            $_POST['sexo_id'], $_POST['rua'], $_POST['numero'], $_POST['cep'], $_POST['telefoneprincipal'],
            $_POST['telefonesecundario']);

        $paciente = new Paciente($_POST['convenios'], $_POST['paciente_antigo_especial'], $_POST['tipo_indicacao'],
            $_POST['altura'], $_POST['peso'], $_POST['pessoaid']);

        $this->pessoaDAO->inserir($pessoa, $paciente);
        $this->form_insercao();
    }

    public function listar()
    {
        $pacientes = $this->pacienteDAO->listar();
        $pessoas = $this->pessoaDAO->listar();
        include_once 'view/paciente/listar.php';

    }

    public function alteracao()
    {
        $pessoa = new Pessoa($_POST['nome'], $_POST['data_nasc'], $_POST['rg'], $_POST['cpf'], $_POST['senha'],
            $_POST['sexo_id'], $_POST['rua'], $_POST['numero'], $_POST['cep'], $_POST['telefoneprincipal'],
            $_POST['telefonesecundario']);

        $paciente = new Paciente($_POST['convenios'], $_POST['paciente_antigo_especial'], $_POST['tipo_indicacao'],
            $_POST['altura'], $_POST['peso'], $_POST['pessoaid']);

        $pessoa->setId($_POST['id']);
        $paciente->setId($_POST['id']);
        $this->pessoaDAO->alterar($pessoa, $paciente);

        $this->form_insercao();
    }

    public function form_alteracao()
    {
        $acao = 'alteracao';
        $rotuloBotao = "Alterar";
        $paciente = $this->pacienteDAO->buscar($_GET['id']);
        $sexos = $this->sexoDAO->listar();
        $pessoas = $this->pessoaDAO->listar();
        include_once 'view/paciente/form.php';

        $acao = 'alteracao';
        $rotuloBotao = "Alterar";
    }

    public function excluir()
    {
        $this->pessoaDAO->excluir($_GET['id']);
        $this->form_insercao();
    }
}