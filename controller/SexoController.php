<?php

include_once './dao/SexoDAO.php';
include_once './model/Sexo.php';

class SexoController
{
    private $sexoDAO;

    public function __construct() {
        $this->sexoDAO = new SexoDAO();
    }

    public function form_insercao() {
        $acao = 'insercao';
        $sexo = null;
        $rotuloBotao = "Inserir";
        include_once 'view/sexo/form.php';
        $this->listar();
    }

    public function form_alteracao() {
        $acao = 'alteracao';
        $rotuloBotao = "Alterar";
        $sexo = $this->sexoDAO->buscar($_GET['id']);
        include_once 'view/sexo/form.php';
    }

    public function exclusao() {
        $this->sexoDAO->excluir($_GET['id']);
        $this->form_insercao();

    }

    public function insercao() {
        $sexo = new Sexo($_POST['descricao'],$_POST['sigla']);
        $this->sexoDAO->inserir($sexo);
        $this->form_insercao();

    }

    public function alteracao() {
        $sexo = new Sexo($_POST['descricao'],$_POST['sigla']);
        $sexo->setId($_POST['id']);
        $this->sexoDAO->alterar($sexo);
        $this->form_insercao();
    }

    public function listar() {
        $sexos = $this->sexoDAO->listar();
        include_once 'view/sexo/listar.php';

    }

}
