<?php

include_once './dao/UFDAO.php';
include_once './dao/PaisDAO.php';

include_once './model/UF.php';

class UFController {

    private $UFDAO;
    private $paisDAO;

    public function __construct() {
        $this->UFDAO = new UFDAO();
        $this->paisDAO = new PaisDAO();
    }

    public function form_insercao() {
        $acao = 'insercao';
        $uf = null;
        $rotuloBotao = "Inserir";
        $paises = $this->paisDAO->listar();
        include_once 'view/uf/form.php';
        $this->listar();
    }

    public function insercao() {
        $uf = new UF($_POST['descricao'], $_POST['sigla'], $_POST['pais_id']);
        $this->UFDAO->inserir($uf);
        $this->form_insercao();
    }

    public function listar() {
        $ufs = $this->UFDAO->listar();
        include_once 'view/uf/listar.php';
    }

    public function alteracao() {
        $uf = new UF($_POST['descricao'], $_POST['sigla'], $_POST['pais_id']);

        $uf->setId($_POST['id']);

        $this->UFDAO->alterar($uf);
        $this->form_insercao();
    }

    public function form_alteracao() {
        $acao = 'alteracao';
        $uf = $this->UFDAO->buscar($_GET['id']);
        $rotuloBotao = "Alterar";
        $paises = $this->paisDAO->listar();
        include_once 'view/uf/form.php';
    }

    public function exclusao() {
        $this->UFDAO->excluir($_GET['id']);
        $this->form_insercao();
    }
}
