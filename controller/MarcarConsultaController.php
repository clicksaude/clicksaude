<?php

include_once 'dao/MarcarConsultaDAO.php';
include_once 'model/MarcarConsulta.php';
include_once 'model/Paciente.php';
include_once 'model/Profissional.php';

class MarcarConsultaController {

    private $marcar_consultaDAO;
    private $pacienteDAO;
    private $profissionalDAO;

    public function __construct() {
        $this->marcar_consultaDAO = new MarcarConsultaDAO();
        $this->pacienteDAO = new PacienteDAO();
        $this->profissionalDAO = new ProfissionalDAO();
       // (new UsuarioController())->verificaUsuario();
    }
    
    public function form_insercao() {
        $acao = 'insercao';
        $marcar_consulta = null;
        $rotuloBotao = "Inserir";
        $pacientes = $this->pacienteDAO->listar();
        $profissionais = $this->profissionalDAO->listar();
        include_once 'view/marcar_consulta/form.php';
        $this->listar();
    }
    
    public function form_alteracao() {
        $acao = 'alteracao';
        $rotuloBotao = "Alterar";
        $anamnese = $this->marcar_consultaDAO->buscar($_GET['id']);
        $pacientes = $this->pacienteDAO->listar();
        $profissionais = $this->profissionalDAO->listar();
        include_once 'view/marcar_consulta/form.php';
    }
    
    public function exclusao() {
        $this->marcar_consultaDAO->excluir($_GET['id']);
        $this->form_insercao();
    }
    
    public function insercao() {
        $id_profissional = $this->profissionalDAO->getProfissional();

        $marcar_consulta = new MarcarConsulta($_POST['data_consulta'], $_POST['paciente_id'], $id_profissional);
        $this->marcar_consultaDAO->inserir($marcar_consulta);
        $this->form_insercao();
    }
    
    public function alteracao() {
        $marcar_consulta = new MarcarConsulta($_POST['data_consulta'], $_POST['paciente_id'], $_POST['profissional_id']);
        $marcar_consulta ->setId($_POST['id']);
        $this->marcar_consultaDAO->alterar($marcar_consulta);
        $this->form_insercao();        
    }
    
    public function listar() {
        $consultas_marcadas = $this->marcar_consultaDAO->listar();
        include_once 'view/marcar_consulta/listar.php';
    }
}
