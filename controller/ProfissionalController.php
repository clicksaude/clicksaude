<?php

include_once './dao/ProfissionalDAO.php';
include_once './dao/PessoaDAO.php';

include_once './model/Profissional.php';

class ProfissionalController {

    private $profissionalDAO;
    private $sexoDAO;
    private $pessoaDAO;

    public function __construct() {
        $this->profissionalDAO = new ProfissionalDAO();
        $this->pessoaDAO = new pessoaDAO();
        $this->sexoDAO = new SexoDAO();
    }

    public function form_insercao() {
        $acao = 'insercao';
        $profissional = null;
        $pessoa = null;
        $rotuloBotao = "Inserir Profissional";
        $pessoas = $this->pessoaDAO->listar();
        $sexos = $this->sexoDAO->listar();
        include_once 'view/profissional/form.php';
        //$this->listar();
    }

    public function insercao()     {
        $pessoa = new Pessoa($_POST['nome'], $_POST['data_nasc'], $_POST['rg'], $_POST['cpf'], $_POST['senha'],
            $_POST['sexo_id'], $_POST['rua'], $_POST['numero'], $_POST['cep'], $_POST['telefoneprincipal'],
            $_POST['telefonesecundario']);

        $profissional = new Profissional($_POST['tipo'],  $_POST['pessoaid']);
        $this->profissionalDAO->inserir($pessoa, $profissional);
        $this->form_insercao();
    }

    public function listar() {
        $profissionais = $this->profissionalDAO->listar();
        $pessoas = $this->pessoaDAO->listar();
        include_once 'view/profissional/listar.php';
    }

    public function alteracao() {

        $pessoa = new Pessoa($_POST['nome'], $_POST['data_nasc'], $_POST['rg'], $_POST['cpf'], $_POST['senha'],
            $_POST['sexo_id'], $_POST['rua'], $_POST['numero'], $_POST['cep'], $_POST['telefoneprincipal'],
            $_POST['telefonesecundario']);

        $profissional = new Profissional($_POST['tipo'], $_POST['pessoaid']);

        $pessoa->setId($_POST['id']);
        $profissional->setId($_POST['id']);

        $this->pessoaDAO->alterar($pessoa, $profissional);
        $this->form_insercao();
    }

    public function form_alteracao() {
        $acao = 'alteracao';
        $rotuloBotao = "Alterar";
        $profissional = $this->profissionalDAO->buscar($_GET['id']);
        $sexos = $this->sexoDAO->listar();
        $pessoas = $this->pessoaDAO->listar();
        include_once 'view/profissional/form.php';
        $pessoas = $this->pessoaDAO->listar();
    }

    public function excluir() {
        $this->profissionalDAO->excluir($_GET['id']);
        $this->form_insercao();
    }

    public function getProfissional(){
        $x = $this->profissionalDAO->getProfissional();
    }
}
