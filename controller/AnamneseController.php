<?php

include_once 'dao/AnamneseDAO.php';
include_once 'model/Anamnese.php';

class AnamneseController {

    private $anamneseDAO;

    
    public function __construct() {
        $this->anamneseDAO = new AnamneseDAO();
       // (new UsuarioController())->verificaUsuario();
    }
    
    public function form_insercao() {
        $acao = 'insercao';
        $anamnese = null;
        $rotuloBotao = "Inserir";
        include_once 'view/anamnese/form.php';
        $this->listar();
    }
    
    public function form_alteracao() {
        $acao = 'alteracao';
        $rotuloBotao = "Alterar";
        $anamnese = $this->anamneseDAO->buscar($_GET['id']);
        include_once 'view/anamnese/form.php';
    }
    
    public function exclusao() {
        $this->anamneseDAO->excluir($_GET['id']);
        $this->form_insercao();
       
    }
    
    public function insercao() {
        $anamnese = new Anamnese($_POST['atividade_fisica'], $_POST['medicamentos_em_uso'],$_POST['queixas'], $_POST['fraturas'],
                $_POST['cirurgia'], $_POST['arquivoPdf_cadastrosAntigos']);
        $this->anamneseDAO->inserir($anamnese);
        $this->form_insercao();     
        
    }
    
    public function alteracao() {
        $anamnese = new Anamnese($_POST['atividade_fisica'], $_POST['medicamentos_em_uso'],$_POST['queixas'], $_POST['fraturas'],
                $_POST['cirurgia'], $_POST['arquivoPdf_cadastrosAntigos']);
        $anamnese ->setId($_POST['id']);
        $this->anamneseDAO->alterar($anamnese);
        $this->form_insercao();        
    }
    
    public function listar() {
        $anamneses = $this->anamneseDAO->listar();
        include_once 'view/anamnese/listar.php';
        
    }

}
