<?php

include_once 'dao/UsuarioDAO.php';

class UsuarioController {

    private $usuarioDAO;

    public function __construct() {
        $this->usuarioDAO = new UsuarioDAO();
           $_SESSION["mensagem"] = "";
    }
    
    public function form_login() {
        $acao = 'login';
        include_once 'view/usuario/form_login.php';
    }
    
    public function login() {
        $usuario = $this->usuarioDAO->buscar($_POST['username'], $_POST['senha']);
        if ($usuario == null){
             $_SESSION["mensagem"] = "Usuário nao existe!";
             header("Location:  ?classe=UsuarioController&acao=form_login");
        } else {
            $this->logar($_POST['username']);
            header("Location:  index.php");
        }        
    }
        
    public function logar($username){
            $_SESSION['username'] = $username;
        $this->iniciaTempoSessao();
    }
    
    public function usuarioLogado() {
        return $_SESSION['username']; 
       
    }    
    
    public function deslogar(){
        session_destroy();
        header("Location:  index.php");
    }
    
    public function usuarioEstaLogado() {
        return isset($_SESSION['username']);
    }
    
     public function sessaoExpirada() {
        if ($_SESSION['tempo'] < time()) {
            $this->deslogar();
            return TRUE;
        } else {
            $this->iniciaTempoSessao();
            return false;
        }
    }

    public function iniciaTempoSessao() {
        $_SESSION['tempo'] = time() + 10;
    }
    
 public function validaUsuario() {
        if (!$this->usuarioEstaLogado() or $this->sessaoExpirada()) {
            $_SESSION["mensagem"] = "Voce nao tem acesso";
            header("Location:  index.php");
        } else {
            $_SESSION["mensagem"] = "ok";
        }
    }

}
