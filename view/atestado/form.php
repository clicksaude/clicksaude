<?php
include_once 'view/cabecalho.php';
?>
<form action="?classe=AtestadoController" method="post">
    <input type="hidden" name="acao" value="<?= $acao ?>">
    <input type="hidden" name="id" value="<?= $atestado['id'] ?>">
    Tipo: <input type="text" autofocus name="tipo" value="<?= $atestado['tipo'] ?>">
    Dias de atestado: <input type="text" autofocus name="dias_atestado" value="<?= $atestado['dias_atestado'] ?>">
    Data da consulta: <input type="date" autofocus name="data_consulta" value="<?= $atestado['data_consulta'] ?>">
    <input type="submit" value="<?= $rotuloBotao ?>">
</form>