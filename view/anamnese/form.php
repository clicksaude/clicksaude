<?php
include_once 'view/cabecalho.php';
?>

<div class="container">
    <form action="?classe=AnamneseController" method="post">
        <input type="hidden" name="acao" value="<?= $acao ?>">
        <input type="hidden" name="id" value="<?= $anamnese['id'] ?>">
        Atividade Fisica: <input type="text" autofocus name="atividade_fisica" value="<?= $anamnese['atividade_fisica'] ?>">
        Medicamentos em Uso: <input type="text" autofocus name="medicamentos_em_uso" value="<?= $anamnese['medicamentos_em_uso'] ?>">
        Queixas: <input type="text" autofocus name="queixas" value="<?= $anamnese['queixas'] ?>">
        Fraturas: <input type="text" autofocus name="fraturas" value="<?= $anamnese['fraturas'] ?>">
        Cirurgias: <input type="text" autofocus name="cirurgia" value="<?= $anamnese['cirurgia'] ?>">
        Atestado em PDF: <input type="text" autofocus name="arquivoPdf_cadastrosAntigos" value="<?= $anamnese['arquivopdf_cadastrosantigos'] ?>">
        <input type="submit" value="<?= $rotuloBotao ?>">
    </form>
</div>
