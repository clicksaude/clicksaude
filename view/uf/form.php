<form action="?classe=UFController" method="post">
    <input type="hidden" name="acao" value="<?= $acao ?>" >
    <input type="hidden" name="id" value="<?= $uf['id'] ?>" >
    Nome: <input type="text" name="descricao" value="<?= $uf['descricao']?>">
    Sigla: <input type="text" name="sigla" value="<?= $uf['sigla']?>">

    País:
    <select name="pais_id">
        <?php
        foreach ($paises as $valor) {
            $selecionado = "";
            if ($valor[id] == $uf[pais_id]){
                $selecionado = "selected";
            }
            echo "<option $selecionado value=$valor[id]>$valor[nome]</option>";
        }
        ?>
    </select>
    <input type="submit" value="<?= $rotuloBotao ?>">

</form>
