<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="background:#378a82;">

<div class="container">

        <div id="login-page" class="row">
            <div class="col s6 offset-s3 card-panel" style="margin-top:10%;">
                <form class="login-form"  action="?classe=UsuarioController" method="post">
                    <input type="hidden" name="acao" value="<?= $acao ?>">
                    <div class="row">
                        <div class="input-field col s12 center">
                            <img width="200" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA/1BMVEVKvJb///+CclpbTz/x7c3WISc/Pz5Hv5iDb1huj3JcRjhQrYpEupM0t407uJDy8NA/OjvVABNDcWHVCBjt1bnw5Mbqw6rlpJBbSzyFbFXWEx9Eg21YfmROtI9rlHWEdFtCRkOGz7XR7OLk9O5iw6J7alJ5y67h8+yj2sY+NDfU7eRMPi9KRkDw+fZnxaWv3s244tPiiXrnrZiT1L1TS0C34tJiWUx6cV5DeWayqI2PgWjUzbDHv6JwZFK6sZXf2ryYi3LecWdbaFNWhWrUAADszbJBXVLYNzhGk3hlnn14gWZHoIJAT0ilmX/jkoLMya9xXEzaSEUvMDOPjX4+LTN+AAEDAAAM3klEQVR4nOXde2PSyBYA8AmyTbXJ0FqrtIpsIFAKBlDaKm0pruv6vNV7e/3+n+VOgISQTDLnTCYkuZ4/dlf7IL89885kQrTMw3H644vexOoObZu4YdvDrjXpXYz7jpP9x5Msf7nTv5gMCTUppQYLsg73j+xvTUqGk4t+ps6shE6rN3RpGy5euFKTDnutrJhZCJ2xRQC2sJNY4yyUyoWtiW1SDC7ApKY9aam+ILXClkUldWsltdQiFQrT8zJBqhK2J4YSnoc0em1FV6ZGOB6a6ngrpDkcK7k2BUKnp6Z0RoyU9hQ0rqmFbSsTnoe0UhfWlMJ2V3nxDBnNbkpjKmG7m2H+fCNNZ0whdLLOn280uynqo7xwsoX8+UY62brwAjXsVGA0LrYqPLXpVn1uUPt0e8LJlirgZhimVFGVELZIHr6FkUiMV/FCy8zJ54ZEGrHC09wSuAyDYGsjUtjLM4HLMHsZCp0cmtBoUBvV/2OE/S33gXFhGP1shAUooV6YoyyE3SKUUC9oV7nQsYtRQr0wwJURKDwtSBVch2EAuw2YsFWkEuoFhQ1wQMKL4rQxwTBB0w2IcFRMIJAIEBYWCOs1xMICdYPRAAzhhMJCAyFEkbDARXQZwoIqEI6LDmREweJ/srAEQCExUdgvA5ARE6caScJ2EUcyvKBJi+IJQifvC0dEwjA8QViw2URSGLaMsFseICPGzxdjhaOyVMJl0NhuMU5YkmZ0HbENaozQKVMRXYYR09rECEvUyngR19rwhb1yVcJlUP4gnCssXSVcBr8qcoV5X6p0QIVW+SrhMgwLJmyVs4y6YXKW3zjCsmbQDQMinKgW6qJQ+FlG9A5qRHiqtozq+vfn7x4nxbsvDYVIM7ISHhHayj7MDf352dm+IM5+fdpTR4z0+2HhWG1f/+lsBxD7Z1+UEWl4TSMsVFoJ9U/7ECCLX9+VEcONTUiotJlhRRQIZKHsU8ODt02ho7aMQjPIQmE5NZ0EodLRjP4FkcL9T+qKqRUvbCvtKfTHiBzu7CvsMdqxQrVLM/qfQQA3gsVU3QdvLtoEhaeKZ4UB39mn55x4vLM2njXUfTA9jRGqXl0LVLJGzIDt3VkWwo0kBoRqayEJCP8VOyxb95gqhRs1MSBUPi3005PQnX8/y0IYbE7XQsV9IQkIky4+EyGhDkfYUz4vhAj1bIRGjyNUv7yWo5DQqFDxpMINkPBXRsJxRDhUv3iBrIcq5/rEGIaF8K6C9WKNPVD8uYr9pG/aX33TznfQ72xAVz38DsMTgqdN+t6LB49g8cCL1N8U/P4XsAUBf8XGE4KBLx7VH+Qa9UcfQAXa2BRCNx/qHx6JryF74x7gUr2tiwQ1ntH/LgCQER8ArtUb1xBUZ9goBJARXwDKKQ0KgYVU/zvnOuhHHXC1q2JKUIX0Q94yLx59F1/tqpgSTCHVC1JIWQ4hC1d0LYS2pEWphtCK2PKF0O6+ZMJlp78QQu9VlEy4vIfhCh3omLRswsXaMMFMnMomXEyhCLyvSC2sD1jUV//chnDRX7hCoC+VsD54cDW9mR9V3ZjfTN9fDtIgYcLF5gyCWYKSFdYHl3fzGovqKtz/nk9n8kig0F2QIpiHmuSE9cHVfI1bB0O+lzVCha2FEH7PUEo4eD/n8FbI+Z2cESh0l9wIZoVGQjiYxfuWxreDDIXDhRC+yIYXDqa88rlpvJUgQlsa6goRtyvwwmuRb5HGS3RJhQrNNhMinp5ECuuzaghYW0Xob6vokgrOYYsJR/CFUpyw/jacrOqz14t4dhQ2XiGJYOGICRG3nFDC+tsw48kfByeLODz5JyURKmSjGoLZBIUSziL17T8nf/gRrp+1t6i6CBWy6QXBbJdFCSPAo49r4EE4idXqZSZC5sPcNtyDCwc/I1l6chAQPo18+RpTTsFC6hDMvW24sH4XyZFAWK1NEUSw0GwTzKP2Dfg1hK9fLKzWZnDh+TtoDlsEc9+wcbbz6hxyAYMbDkAoBJfT8519sHBMEN2hK2QhNkY6CpCQdRmQ9vT8lbt/BSo0RgRz+76xup0pSmS0mQEJq3NhEhc8lLBHMPstG4GteEnIGe/yAUJBp+jxUMIJweyiafx7/RkJmRzcygp/xifxPPjRCKFFunAgaRw3Z+fBD+Ir65yLBwmrNRBvZ+dy9yG0x++SIUpYqTQrlzsC5BX/6iHCO04xDfNezSpNuNAmmL35rpAZw4lkHyoupDDhTbiYhnSs/s+a7BqyFVYWiQx/9DqVg/n6ioMREgZjbQwKw8lbps8FooSY8IUushNBekrvgmvVN88C8SMwtTh5HfzKG3+m7I9rorqdV5eVpvfpcCEugkL2P5OHZEqvu3fng4eBCAAZMfiVA3++WHtf5+pY49JpNtcfvhVhHPL+v97Vft4wJcaJl8Mpb2/4K5fXDH7ytoRLZGWz4bmfLoW1fw7EsnDLU7u55xXOTd52hauGZ7ZO5f1q1F17ihAevln90M/7Td2sEsahhVJtKQfpK+9/phBWr+83dFweSijZWyQp0wnnC+F5ki5H4VLZua6mES5Tl6TDCYfoUZswOtepctgR4ZDCLm5uARKmaWmq1x0IEC5kcwvU/BAk9IalRwjhxydeb6FaOMHN8SHCXW+Zrfb6o5i2jIO/vJ+Zqhb2cOs0EGHFnzzVnn0+CUbQtPGFzz/8H7kDARHCEW6tDSTsBKYWR4EIjnEOnwa/EphcgNoZhJCOceulMOF1lReg+eEcVkgRwhZqzRsoTDMDVi0026j7FjBhJcUqxnsYEJFDB3fvCSbszLlXDxBWgSlE9PjI+4dA4VR2NRFaSOFCG3kPGCasVCRzWAO2pHDh4h4w5j4+UMhtawD3LcAphAtHyL0Y0Bzykgi5MwP99WDhYi8GZj8NVMirieK7a7fgFIKFi/00mD1R4BxymlOh8AgOhOcQu68NLOT0icK73NC+ECFc7WtDzJ/gwk7kRr5opwJwVrESvgQ/oofaX4oQ+hPhQByuhR8ju6KAU1+ccLW/FDFuQwj91Yw14rWfRH8+6H8NOuRGCh3kPm+UkLU2YcaPk4NFHL6OAFE+sJBg9+rjhJwsVp8sIrJlcb6bidDfqw+fBOOEnLrI23yJrYNwof+8BfiZGayQtajhdHEC14pihP4zM/DpBVZY6TRFu4Rr8ys8ECi00c+u4YWiNNaqdxI+oDDw7Bq4R5QQuoPUcOe39t12pIAwYeD5Q/DQVEpY6XTurqObu92nZnblfFAh/jlgSaGLbE5v5oGNC+6jT03J/EGFG88BQ4uptNBFdipXd9Pbm5vb6d0V+5M8DyjceJYbWkzTCFdMN7D9u6RQ5kyF1EJVARCGzlQAFtMyCUPnYsDONtHLJAydbQLr9HXwal/WsftVJIycTwNaj9Ifpm8j1MSx8FSpyBlDoHcF6HsFKaa734QpjJwTBZtC6d+KkcRj4RE8nLO+YF1ioxDCY/FKG+e8NtiZe/peJXfjLgDIPXMPtiClk5eV49084/gb4LAv7rmJ4GN4yN7Xlw9zipcvv0IO+uKffYk7lC6/gFxgzPml5XrDU1LEnUGr/Bzh3CL2HOH/lyTGnwWt/iThfCLhPO/yvmEmGElnsmdwlnAOQZPO1S/nq6w2I/ndCOV+jc4yBO+3yOI44e2G8B0lZXynXDCi75fL+l1B2w7Au4LUv+9pmwF531O5GxvQO7t+g/eulXdkA3133m/w/sPf4B2W5Ry8Yd5DWsp+H/cu2d/gfcDlq4rYdzr/Bu/lLtmijcy71UvV2sS1MgKhk/d1IyKmlREItXZZqiJtJyiShGUZg/PG20ChNi4D0QyvW2CE2kXxiQKgSKiNik40YztCoFDrFZto8ofbGGGxiWIgQFjkgiosojBhcVtUUSMDFhaVCALChFq/iKMbGjdfkhFqbVK0YbhBkoZqeKHmFGymYdgJg20pIZsvFqmk0vj5oLywSB0joBuUEWotoxgl1TASJxMphKwyFqGkUnAVxAsLUVIxJVRCqPVz7jYMAusF5YWaZuWZRtNCXy9eqLVIXrWREkwTIy/UtAnNo6gaNHoLOyuh1s6hUaU2cJimRMimG1suqpSAJhIKhe49xu0VVSPm3mC2Qs3pmtsxGmYX1ccrE7Lq2N1CHg3alauAKoSadpp1Hln+IructipkebQyzKNBrVT5UyJk9bFHM0EalPZS1D+FQhbjofLCaphD2f5hM9QIWWGdGAoTyX7XJHXxXIUqIYuWpaa0stJpSYw/40KhUFOBVMzTVAtZtCa2Kak0qGlP1PK0DIQsnLFFWC4xTIPljlhjBU1nJLIQuuG0ekNqQsoss5l02GtloXMjK+EinNbIsokLZRndtBpu1thXiG2NMsMtIlPhMpx2fzzqTayubS8PM2L/7lqT3mjcb2dqW8b/AGMx6gkeGIHWAAAAAElFTkSuQmCC" alt="" class="circle responsive-img valign profile-image-login">
                            <p class="center login-form-text" style="font-size:30px;">Click Saúde - Login</p>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s10 offset-s1">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="username" type="text" autofocus name="username">
                            <label for="username" class="center-align">Username</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s10 offset-s1">
                            <i class="material-icons prefix">lock_outline</i>
                            <input id="password" type="password"  name="senha">
                            <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s10 offset-s1">
                            <input type="checkbox" id="remember-me" />
                            <label for="remember-me">Lembrar-me</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6 offset-s3">
                            <input type="submit" class="btn waves-effect waves-light col s12" value="Acessar">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6 ">
                            <p class="margin medium-small"><a href="page-register.html">Cadastre-se</a></p>
                        </div>
                        <div class="input-field col s6 m6 l6">
                            <p class="margin right-align medium-small"><a href="page-forgot-password.html">Esqueceu sua senha ?</a></p>
                        </div>
                    </div>
            </div>
        </div>
    </form>
</div>



