<?php
include_once 'view/cabecalho.php';
?>

<div class="container container-paciente">
    <div class="row">
        <div class="col s6">
            <h3 style="padding-left:10px;" class="lighter">Cadastro de Profissional</h3>
        </div>
    </div>
    <form action="?classe=ProfissionalController" method="post">

        <div class="row">
            <div class="col s6">
                <input type="hidden"    name="acao" value="<?= $acao ?>" >
                <input type="hidden"    name="pessoaid" value="<?= $pessoa['id']?>">
                <input type="hidden"    name="profissionalid" value="<?= $profissional['id'] ?>" >

                <div class="input-field col s12">
                    <input type="text" autofocus name="nome" value="<?= $pessoa['nome']?>">
                    <label for="nome">Nome</label>
                </div>


                <div class="input-field col s12">
                    <input type="date" class="datepicker" name="data_nasc" value="<?= $pessoa['data_nasc']?>">
                    <label for="data_nasc">Data de nascimento</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" name="rg" value="<?= $pessoa['rg']?>">
                    <label for="RG">RG</label>
                </div>


                <div class="input-field col s12">
                    <input type="text" name="cpf" value="<?= $pessoa['cpf']?>">
                    <label for="cpf">CPF</label>
                </div>


                <div class="input-field col s12">
                    <input type="password" name="senha" value="<?= $pessoa['senha'] ?>">
                    <label for="senha">Crie sua Senha</label>
                </div>


                <div class="input-field col s12">
                    <select name="sexo_id">
                        <?php
                        foreach ($sexos as $valor) {
                            $selecionado = "";

                            if ($valor[id] == $pessoa[sexo_id]){
                                $selecionado = "selected";
                            }
                            echo "<option $selecionado value=$valor[id]>$valor[descricao]</option>";
                        }
                        ?>
                    </select>
                    <label>Sexo</label>
                </div>
            </div>

           <div class="col s6">
               <div class="input-field col s12">
                   <input type="text"  name="rua" value="<?= $pessoa['rua']?>">
                   <label for="cpf">Rua</label>
               </div>


               <div class="input-field col s12">
                   <input type="text" name="numero" value="<?= $pessoa['numero']?>">
                   <label for="cpf">Numero</label>
               </div>


               <div class="input-field col s12">
                   <input type="text" name="cep" value="<?= $pessoa['cep']?>">
                   <label for="cep">CEP</label>
               </div>


               <div class="input-field col s12">
                   <input type="text" name="telefoneprincipal" value="<?= $pessoa['telefoneprincipal']?>">
                   <label for="telefoneprincipal">Telefone Principal</label>
               </div>


               <div class="input-field col s12">
                   <input type="text" name="telefonesecundario" value="<?= $pessoa['telefonesecundario']?>">
                   <label for="telefonesecundario">Telefone Secundario</label>
               </div>


               <div class="input-field col s12">
                   <input type="text" name="tipo" value="<?= $profissional['tipo']?>">
                   <label for="tipo">Tipo de profissional</label>
               </div>
           </div>
            <div class="row">
                <div class="input-field col s12">
                    <input class="col s4 offset-s4 waves-effect waves-light blue btn teal lighten-2" type="submit" value="<?= $rotuloBotao ?>">
                </div>
            </div>
        </div>

    </form>
</div>
