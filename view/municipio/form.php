<form action="?classe=MunicipioController" method="post">
    <input type="hidden" name="acao" value="<?= $acao ?>" >
    <input type="hidden" name="id" value="<?= $municipio['id'] ?>" >
    Nome: <input type="text" name="nome" value="<?= $municipio['nome']?>">

    Unidade Federativa:
    <select name="unidade_federativa_id">
        <?php
        foreach ($ufs as $valor) {
            $selecionado = "";
            if ($valor[id] == $municipio[unidade_federativa_id]){
                $selecionado = "selected";
            }
            echo "<option $selecionado value=$valor[id]>$valor[descricao]</option>";
        }
        ?>
    </select>
    <input type="submit" value="<?= $rotuloBotao ?>">

</form>
