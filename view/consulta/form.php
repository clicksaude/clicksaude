<?php
include_once 'view/cabecalho.php';
?>

<form action="?classe=ConsultarController" method="post">
    <input type="hidden" name="acao" value="<?= $acao ?>">
    <input type="hidden" name="id" value="<?= $consulta['id']?>">

    <div class="row">
        <form class="col s12">
            <div class="row">
                <div class="input-field col s4 offset-s4">
                    <input type="text" autofocus name="historico_clinico" value="<?= $consulta['historico_clinico']?>">
                    <label for="historico_clinico">Histórico Clinico</label>
                </div>

                <div class="input-field col s4 offset-s4">
                    <input type="text" autofocus name="foto" value="<?= $consulta['foto']?>">
                    <label for="foto">Foto</label>
                </div>

                <div class="input-field col s4 offset-s4">
                    <input type="text" autofocus name="video" value="<?= $consulta['video']?>">
                    <label for="video">Vídeo</label>
                </div>
            </div>

            <input type="submit" value="<?= $rotuloBotao ?>" <a class="waves-effect waves-light btn col s2 offset-s4"> </a>
        </form>
    </div>
</form>
<form action="?classe=AnamneseController&acao=form_insercao" method="post">
    <div class="row">
        <input type="submit" value="Anamanse"<a class="waves-effect waves-light red btn s2 offset-s4" ></a>
    </div>
</form>
<form action="?classe=AtestadoController&acao=form_insercao" method="post">
    <div class="row">
        <input type="submit" value="Atestado"<a class="waves-effect waves-light blue btn s2 offset-s4" ></a>
    </div>
</form>


