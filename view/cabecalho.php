<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/main.css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<nav>
    <div class="nav-wrapper">
        <a href="?" class="brand-logo" style="font-family: "Roboto">
            <i class="fa fa-heartbeat material-icons" aria-hidden="true"></i>
            Click Saúde <span style="font-size:20px;" > - revitalizando sua vida!</span>
        </a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li>
                <a href="?classe=PacienteController&acao=form_insercao" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para realizar o cadastro">
                <i class="fa fa-wheelchair material-icons left" aria-hidden="true"></i>
                    Cadastro Paciente
                </a>
            </li>

            <li>
                <a href="?classe=ProfissionalController&acao=form_insercao" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para realizar o cadastro">
                <i class="fa fa-handshake-o material-icons left" aria-hidden="true"></i>
                    Cadastro Profissional
                </a>
            </li>

            <li>
                <a href="?classe=MarcarConsultaController&acao=form_insercao" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para marcar uma consulta">
                <i class="fa fa-id-card-o material-icons left" aria-hidden="true"></i>
                    Marcar Consulta
                </a>
            </li>

            <li>
                <a href="?classe=UsuarioController&acao=deslogar" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Clique para sair do sistema">
                <i class="fa fa-sign-out material-icons left" aria-hidden="true"></i>
                    Sair do Sistema
                </a>
            </li>

        </ul>
    </div>
</nav>





