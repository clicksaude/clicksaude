<div class="container">
    <div class="row">
        <div class="center col s6 offset-s3">
            <h2>Bem vindo ao Click Saúde!</h2>
        </div>
    </div>

    <div class="row">
        <div class="col s6">
            <h3 class="lighter">O que você deseja fazer?</h3>
        </div>
    </div>

    <div class="row">
        <div class="col s4 ">
            <div class="card card-dark-green">
                <a class="white-text" href="?classe=PacienteController&acao=form_insercao">
                    <div class="card-content white-text">
                        <h4 class="center">
                            <i class="fa fa-wheelchair material-icons" aria-hidden="true"></i>
                            Paciente
                        </h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="col s4 ">
            <div class="card card-light-green">
                <a class="white-text" href="?classe=MarcarConsultaController&acao=form_insercao">
                    <div class="card-content white-text">
                            <h4 class="center">
                                <i class="fa fa-id-card-o material-icons" aria-hidden="true"></i>
                                Marcar consulta
                            </h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="col s4">
            <div class="card card-dark-green">
                <a class="white-text" href="?classe=ProfissionalController&acao=form_insercao">
                    <div class="card-content white-text">
                        <h4 class="center">
                            <i class="fa fa-handshake-o" aria-hidden="true"></i>
                            Profissional
                        </h4>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s6 ">
            <div class="card card-light-green">
                <a class="white-text" href="">
                    <div class="card-content white-text">
                        <h4 class="center">
                            <i class="fa fa-file-o material-icons" aria-hidden="true"></i>
                            Atestado
                        </h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="col s6 ">
            <div class="card card-light-green">
                <a class="white-text" href="?classe=UsuarioController&acao=deslogar">
                    <div class="card-content white-text">
                        <h4 class="center">
                            <i class="fa fa-sign-out material-icons" aria-hidden="true"></i>
                            Sair do sistema
                        </h4>
                    </div>
                </a>
            </div>
        </div>
    </div>

</div>


