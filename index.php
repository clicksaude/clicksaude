<?php
session_start();
ini_set("display_errors", TRUE);

include_once './controller/DefaultController.php';
include_once './controller/SexoController.php';
include_once './controller/PaisController.php';
include_once './controller/UFController.php';
include_once './controller/MunicipioController.php';
include_once './controller/PessoaController.php';
include_once './controller/UsuarioController.php';
include_once './controller/AnamneseController.php';
include_once './controller/AtestadoController.php';
include_once './controller/PessoaController.php';
include_once './controller/PacienteController.php';
include_once './controller/ProfissionalController.php';
include_once './controller/MarcarConsultaController.php';
include_once './controller/ConsultarController.php';

if(isset($_SESSION['mensagem'])){
    echo $_SESSION['mensagem'];
}

if(!isset($_GET['classe'])){
    $classeControler = new DefaultController();
}

if (isset($_GET['classe'])) {
    $classe = $_GET['classe'];
    $classeControler = new $classe();
    if (isset($_GET['acao'])) {
        $acao = $_GET['acao'];
        $classeControler->$acao();
    } elseif (isset($_POST['acao'])) {
        $acao = $_POST['acao'];
        $classeControler->$acao();
    }
    }

include_once './view/rodape.php';