create table perfil (
  id serial primary key,
  nivel_perfil varchar(100)
);

create table usuario(
  id serial primary key,
  username varchar(100),
  senha varchar(100),
  pessoa_id integer REFERENCES pessoa(id)
);

create table perfil_usuario(
  perfil_id integer references perfil(id),
  usuario_id integer references usuario(id),
  primary key (perfil_id, usuario_id)
);

create table uf (
  id serial primary key,
  nome varchar(100)
);

create table cidade(
  id serial primary key,
  nome varchar(100),
  uf_id integer references uf(id)
);

create table endereco (
  id serial primary key,
  rua varchar (100),
  cep varchar(100),
  numero integer,
  cidade_id integer references cidade(id)
);

create table sexo(
  id serial primary key,
  sigla varchar(100),
  descricao varchar(100)
);

create table pessoa (
  id serial primary key,
  nome varchar (100)not null,
  rg varchar  (20),
  data_nasc date,
  cpf varchar (100),
  telefoneprincipal varchar(30),
  telefonesecundario VARCHAR(30),
  endereco_id integer references endereco(id)
  sexo_id INTEGER REFERENCES sexo(id)
);

create table telefone (
  id serial primary key,
  numero varchar (100),
  pessoa_id integer references pessoa(id)
);

create table profissional (
  id serial primary key,
  tipo varchar(100) not null,
  pessoa_id integer references pessoa(id)
);

create table paciente (
  id serial primary key,
  convenios varchar (100),
  paciente_antigo_especial boolean,
  tipo_indicacao varchar(200),
  altura real,
  peso real,
  pessoa_id integer references pessoa(id)
);

create table atestado (
  id serial primary key,
  tipo varchar (100)not null,
  dias_atestado varchar (100),
  data_consulta date
);

create table anamnese(
  id serial primary key,
  atividade_fisica varchar(300),
  medicamentos_em_uso varchar(300),
  queixas varchar(300),
  fraturas varchar(300),
  cirurgia varchar(300),
  arquivoPdf_cadastrosAntigos varchar(100)
);

create table consulta (
  id serial primary key,
  data_consulta date,
  historico_clinico varchar (100),
  foto VARCHAR(400),
  video VARCHAR(400),
  paciente_id integer references paciente(id),
  profissional_id integer references profissional(id),
  atestado_id integer references atestado(id),
  anamnese_id integer references anamnese(id)
);



