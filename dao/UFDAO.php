<?php

require_once 'dao/Conexao.php';

class UFDAO {

    private $conexao;

    public function __construct() {
        $this->conexao = Conexao::conectar();
    }

    public function excluir($id) {
        $sql = "delete from unidade_federativa where id = $id";
        pg_query($this->conexao, $sql);
    }

    public function inserir(UF $uf) {
        $sql1 = "insert into unidade_federativa (descricao, sigla, pais_id) values "
            . "('{$uf->getDescricao()}', '{$uf->getSigla()}', {$uf->getPais()})";
         pg_query($this->conexao, $sql1);
    }

    public function listar() {
        $ufs = array();
        $sql = "select unidade_federativa.*, pais.nome from unidade_federativa"
            . "  inner join pais on unidade_federativa.pais_id = pais.id ";

        $retorno = pg_query($this->conexao, $sql);
        while ($uf = pg_fetch_array($retorno)) {
            array_push($ufs, $uf);
        }
        return $ufs;
    }

    public function alterar(UF $uf) {
        $sql = "update unidade_federativa set descricao = '{$uf->getDescricao()}', "
            . " sigla = '{$uf->getSigla()}', pais_id = {$uf->getPais()} where id = {$uf->getId()} ";

        pg_query($this->conexao, $sql);
    }

    public function buscar($id) {
        $sql = "select * from unidade_federativa where unidade_federativa.id = $id";
        $retorno = pg_query($this->conexao, $sql);
        return pg_fetch_array($retorno);
    }

}
