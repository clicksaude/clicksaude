<?php

require_once 'dao/Conexao.php';

class PacienteDAO {

    private $conexao;

    public function __construct() {
        $this->conexao = Conexao::conectar();
    }

    public function excluir($id) {
        $sql = "delete from paciente where id = $id";
        pg_query($this->conexao, $sql);
    }

    public function inserir(Paciente $paciente) {
        $sql1 = "insert into paciente (convenios, paciente_antigo_especial,tipo_indicacao,"
                . "altura,peso,pessoa_id) values "
                . "('{$paciente->getConvenios()}', '{$paciente->getPaciente_antigo_especial()}', "
                . "'{$paciente->getTipo_indicacao()}', '{$paciente->getAltura()}','{$paciente->getPeso()}',"
                . " {$paciente->getPessoa()}) returning id";


        $retorno_paciente = pg_query($this->conexao, $sql1);

        $paciente_id = pg_fetch_array($retorno_paciente);

    }

    public function listar() {
        $pacientes = array();
        $sql = "select paciente.*, pessoa.nome  from paciente "
                . "  inner join pessoa on paciente.pessoa_id = pessoa.id  ";
        $retorno = pg_query($this->conexao, $sql);
        while ($paciente = pg_fetch_array($retorno)) {
            array_push($pacientes, $paciente);
        }
        return $pacientes;
    }

    public function alterar(Paciente $paciente) {
        $sql = "update paciente set convenios = '{$paciente->getConvenios()}', "
                . " paciente_antigo_especial = '{$paciente->getPaciente_antigo_especial()}',"
                . " tipo_indicacao = '{$paciente->getTipo_indicacao()}',"
                . " altura = '{$paciente->getAltura()}', peso = '{$paciente->getPeso()}', pessoa_id = '{$paciente->getPessoa()}'"
                . " where id = {$paciente->getId()}";
                
        echo $sql;
        pg_query($this->conexao, $sql);
    }  

    public function buscar($id) {
        $sql = "select paciente.* , pessoa.nome from paciente inner join pessoa on pessoa.id = paciente.pessoa_id where paciente.id = $id";
        $retorno = pg_query($this->conexao, $sql);
        return pg_fetch_array($retorno);
    }

}





