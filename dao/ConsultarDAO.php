<?php

include_once 'dao/Conexao.php';

class ConsultarDAO {

    private $conexao;

    public function __construct() {
        $this->conexao = Conexao::conectar();
    }

    public function alterar(Consulta $consulta) {
        $sql = "update consulta set historico_clinico = '{$consulta->getHistoricoClinico()}', foto = '{$consulta->getFoto()}', " .
            "video = '{$consulta->getVideo()}' where id = {$consulta->getId()}";;

        pg_query($this->conexao, $sql);

    }


    public function listar() {
        $consultas_marcadas = array();
        $sql = "select * from consulta";
        $retorno = pg_query($this->conexao, $sql);
        while ($marcar_consulta = pg_fetch_array($retorno)) {
            array_push($consultas_marcadas, $marcar_consulta);
        }
        return $consultas_marcadas;
    }

    public function buscar($id) {
        $sql = "select * from consulta where id = $id";
       $retorno = pg_query($this->conexao, $sql);
       return pg_fetch_array($retorno);
   }

}

