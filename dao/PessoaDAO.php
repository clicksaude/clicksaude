<?php

require_once 'dao/Conexao.php';

class PessoaDAO {

    private $conexao;
    private $usuarioDAO;

    public function __construct() {
        $this->conexao = Conexao::conectar();
        $this->usuarioDAO = new UsuarioDAO();
    }

    public function excluir($id) {
        $sql = "delete from pessoa where id = $id";
        //AO EXCLUIR UMA PESSOA EXCLUIR UM ENDERECO;
        pg_query($this->conexao, $sql);
    }


    /**
     * @param Pessoa $pessoa
     * @param Paciente $paciente
     */
    public function inserir(Pessoa $pessoa)  {

   //     $sql3 = "insert into profissional (tipo) values "
       //     . "('{$pessoa->getProfissional()->getTipo()}') returning id";

   //     $retorno_profissional = pg_query($this->conexao, $sql3);

     //   $profissional_id = pg_fetch_array($retorno_profissional);

        $sql2 = "insert into endereco (rua, numero, cep) values "
            . "('{$pessoa->getEndereco()->getNumero()}', {$pessoa->getEndereco()->getRua()}, '{$pessoa->getEndereco()->getCep()}') returning id";

        $retorno_endereco = pg_query($this->conexao, $sql2);

        $endereco_id = pg_fetch_array($retorno_endereco);


        $sql1 = "insert into pessoa (nome, data_nasc, rg, cpf, sexo_id, endereco_id, telefoneprincipal, telefonesecundario) values "
            . "('{$pessoa->getNome()}', '{$pessoa->getDataNascimento()}', '{$pessoa->getRg()}', '{$pessoa->getCpf()}', {$pessoa->getSexo()}, 
            {$endereco_id[0]},'{$pessoa->getTelefonePrincipal()}', '{$pessoa->getTelefoneSecundario()}') returning id";

        $retorno_pessoa = pg_query($this->conexao, $sql1);

        $pessoa_id = pg_fetch_array($retorno_pessoa);

        $this->usuarioDAO->inserir($pessoa->getUsuario()->getUsername(), $pessoa->getUsuario()->getSenha(), $pessoa_id['id']);

      // $sql_paciente = "insert into paciente (convenios, paciente_antigo_especial,tipo_indicacao,"
       //     . "altura, peso , pessoa_id) values "
        //    . "('{$paciente->getConvenios()}', '{$paciente->getPaciente_antigo_especial()}', "
        //   . "'{$paciente->getTipo_indicacao()}', '{$paciente->getAltura()}','{$paciente->getPeso()}',"
         //   . "{$pessoa_id['id']}) returning id";


     //   $retorno_paciente = pg_query($this->conexao, $sql_paciente);

     //   $paciente_id = pg_fetch_array($retorno_paciente);
    }

    public function listar() {
        $pessoas = array();
        $sql = "select pessoa.*, endereco.rua, endereco.numero, sexo.sigla ss, usuario.username from pessoa 
                inner join endereco on pessoa.endereco_id = endereco.id
                inner join sexo on pessoa.sexo_id = sexo.id
                inner join usuario on pessoa.id = usuario.pessoa_id";
        $retorno = pg_query($this->conexao, $sql);
        while ($pessoa = pg_fetch_array($retorno)) {
            array_push($pessoas, $pessoa);
        }
        return $pessoas;
    }

    public function alterar(Pessoa $pessoa) {


    //    $sql3 = "update pessoa set
       //         nome = '{$pessoa->getNome()}',
       //         rg = '{$pessoa->getRg()}',
       //         cpf = '{$pessoa->getCpf()}',
       //         sexo_id = {$pessoa->getSexo()},
         //       telefoneprincipal = '{$pessoa->getTelefonePrincipal()}',
         //       telefonesecundario = '{$pessoa->getTelefoneSecundario()}' where id = {$pessoa->getId()} returning profissional_id";



      //  $retorno_profissional = pg_query($this->conexao, $sql3);

     //   $profissional_id = pg_fetch_array($retorno_profissional);

        $sql2 = "update pessoa set 
                nome = '{$pessoa->getNome()}',
                rg = '{$pessoa->getRg()}',
                cpf = '{$pessoa->getCpf()}',
                sexo_id = {$pessoa->getSexo()},
                data_nasc = '{$pessoa->getDataNascimento()}',
                telefoneprincipal = '{$pessoa->getTelefonePrincipal()}',
                telefonesecundario = '{$pessoa->getTelefoneSecundario()}' where id = {$pessoa->getId()} returning endereco_id";

        $retorno_endereco = pg_query($this->conexao, $sql2);

        $endereco_id = pg_fetch_array($retorno_endereco);

        $sql = "update endereco set 
                rua = '{$pessoa->getEndereco()->getNumero()}',
                 numero = {$pessoa->getEndereco()->getRua()},
                 cep = '{$pessoa->getEndereco()->getCep()}'
                 where id = {$endereco_id[0]}
                 ";

        pg_query($this->conexao, $sql);

        $this->usuarioDAO->alterar($pessoa->getUsuario()->getUsername(), $pessoa->getUsuario()->getSenha(), $pessoa->getId());

        $sql3 = "update paciente set
                convenios = '{$paciente->getConvenios()}',
                paciente_antigo_especial = '{$paciente->getPaciente_antigo_especial()}',
                tipo_indicacao = '{$paciente->getTipo_indicacao()}',
                 altura = '{$paciente->getAltura()}',
                 peso = '{$paciente->getPeso()}',
               where id = {$pessoa->getId()}
                 ";

        pg_query($this->conexao, $sql3);
    }

    public function buscar($id) {

    //    $sql = "select pessoa.id, pessoa.nome, pessoa.rg, pessoa.cpf, pessoa.data_nasc, pessoa.telefoneprincipal, pessoa.telefonesecundario,
        //        endereco.rua, endereco.cep, endereco.numero, paciente.convenios, paciente.paciente_antigo_especial, paciente.tipo_indicacao,
        //        paciente.altura, paciente.peso";

        $sql = "select pessoa.id, pessoa.nome, pessoa.rg, pessoa.cpf, pessoa.data_nasc,pessoa.telefoneprincipal, pessoa.telefonesecundario, 
                endereco.rua, endereco.cep, endereco.numero

                from pessoa inner join endereco on endereco.id = pessoa.endereco_id
                            inner join usuario on pessoa.id = usuario.pessoa_id
                            INNER JOIN paciente on pessoa.id = paciente.pessoa_id 
                where pessoa.id = $id";
        $retorno = pg_query($this->conexao, $sql);
        return pg_fetch_array($retorno);
    }

}
