<?php

require_once 'dao/Conexao.php';

class MunicipioDAO {

    private $conexao;

    public function __construct() {
        $this->conexao = Conexao::conectar();
    }

    public function excluir($id) {
        $sql = "delete from municipio where id = $id";
        pg_query($this->conexao, $sql);
    }

    public function inserir(Municipio $municipio) {
        $sql1 = "insert into municipio (nome, unidade_federativa_id) values "
            . "('{$municipio->getNome()}', {$municipio->getUf()})";
        pg_query($this->conexao, $sql1);
    }

    public function listar() {
        $municipios = array();
        $sql = "select municipio.*, unidade_federativa.descricao from municipio 
                inner join unidade_federativa on municipio.unidade_federativa_id = unidade_federativa.id";

        $retorno = pg_query($this->conexao, $sql);
        while ($municipio = pg_fetch_array($retorno)) {
            array_push($municipios, $municipio);
        }
        return $municipios;
    }

    public function alterar(Municipio $municipio) {
        $sql = "update municipio set nome = '{$municipio->getNome()}', "
            . " unidade_federativa_id = {$municipio->getUf()} where id = {$municipio->getId()} ";

        pg_query($this->conexao, $sql);
    }

    public function buscar($id) {
        $sql = "select * from municipio where municipio.id = $id";
        $retorno = pg_query($this->conexao, $sql);
        return pg_fetch_array($retorno);
    }

}
