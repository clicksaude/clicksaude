<?php

include_once 'dao/Conexao.php';

class MarcarConsultaDAO {

    private $conexao;

    public function __construct() {
        $this->conexao = Conexao::conectar();
    }

    public function inserir(MarcarConsulta $marcarConsulta) {
        $sql = "insert into consulta (data_consulta, paciente_id, profissional_id) values ('{$marcarConsulta->getDataConsulta()}',"
        . "  {$marcarConsulta->getPaciente()}, {$marcarConsulta->getProfissional()})";

        pg_query($this->conexao, $sql);

    }

    public function excluir($id) {
        $sql = "delete from consulta where id = $id";
        pg_query($this->conexao, $sql);
    }

    public function alterar(Anamnese $anamnese) {
        $sql = "update anamnese set atividade_fisica = '{$anamnese->getAtividade_fisica()}', medicamentos_em_uso = '{$anamnese->getMedicamentos_em_uso()}', queixas = '{$anamnese->getQueixas()}',"
        . "fraturas = '{$anamnese->getFraturas()}', cirurgia = '{$anamnese->getCirurgia()}', arquivoPdf_cadastrosAntigos ="
        . "'{$anamnese->getArquivoPdf_cadastrosAntigos()}' "
                . " where id = {$anamnese->getId()}";
        pg_query($this->conexao, $sql);
    }

    public function listar() {
        $consultas_marcadas = array();
        $sql = "select * from consulta";
        $retorno = pg_query($this->conexao, $sql);
        while ($marcar_consulta = pg_fetch_array($retorno)) {
            array_push($consultas_marcadas, $marcar_consulta);
        }
        return $consultas_marcadas;
    }

    public function buscar($id) {
        $sql = "select * from consulta where id = $id";
       $retorno = pg_query($this->conexao, $sql);
       return pg_fetch_array($retorno);
   }

}

