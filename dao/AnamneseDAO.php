<?php

include_once 'dao/Conexao.php';

class AnamneseDAO {

    private $conexao;

    public function __construct() {
        $this->conexao = Conexao::conectar();
    }

    public function inserir(Anamnese $anamnese) {
        $sql = "insert into anamnese (atividade_fisica,medicamentos_em_uso,queixas,fraturas,cirurgia,arquivoPdf_cadastrosAntigos) values ('{$anamnese->getAtividade_fisica()}',"
        . "  '{$anamnese->getMedicamentos_em_uso()}'"
        . " , '{$anamnese->getQueixas()}','{$anamnese->getFraturas()}','{$anamnese->getCirurgia()}','{$anamnese->getArquivoPdf_cadastrosAntigos()}' )";
        pg_query($this->conexao, $sql);
    }

    public function excluir($id) {
        $sql = "delete from anamnese where id = $id";
        pg_query($this->conexao, $sql);
    }

    public function alterar(Anamnese $anamnese) {
        $sql = "update anamnese set atividade_fisica = '{$anamnese->getAtividade_fisica()}', medicamentos_em_uso = '{$anamnese->getMedicamentos_em_uso()}', queixas = '{$anamnese->getQueixas()}',"
        . "fraturas = '{$anamnese->getFraturas()}', cirurgia = '{$anamnese->getCirurgia()}', arquivoPdf_cadastrosAntigos ="
        . "'{$anamnese->getArquivoPdf_cadastrosAntigos()}' "
                . " where id = {$anamnese->getId()}";
        pg_query($this->conexao, $sql);
    }

    public function listar() {
        $anamneses = array();
        $sql = "select * from anamnese ";
        $retorno = pg_query($this->conexao, $sql);
        while ($anamnese = pg_fetch_array($retorno)) {
            array_push($anamneses, $anamnese);
        }
        return $anamneses;
    }

    public function buscar($id) {
        $sql = "select * from anamnese where id = $id";
       $retorno = pg_query($this->conexao, $sql);
       return pg_fetch_array($retorno);
   }

}

