<?php

include_once 'dao/Conexao.php';

class AtestadoDAO {

    private $conexao;

    public function __construct() {
        $this->conexao = Conexao::conectar();
    }

    public function inserir(Atestado $atestado) {
        $sql = "insert into atestado (tipo,dias_atestado,data_consulta) values ('{$atestado->getTipo()}','{$atestado->getDias_atestado()}'"
        . ", '{$atestado->getData_consulta()}')";
        pg_query($this->conexao, $sql);
    }

    public function excluir($id) {
        $sql = "delete from atestado where id = $id";
        pg_query($this->conexao, $sql);
    }

    public function alterar(Atestado $atestado) {
        $sql = "update atestado set tipo = '{$atestado->getTipo()}', dias_atestado = '{$atestado->getDias_atestado()}', data_consulta "
        . " = '{$atestado->getData_consulta()}'"
                . " where id = {$atestado->getId()}";
        pg_query($this->conexao, $sql);
    }

    public function listar() {
        $atestados = array();
        $sql = "select * from atestado";
        $retorno = pg_query($this->conexao, $sql);
        while ($atestado = pg_fetch_array($retorno)) {
            array_push($atestados, $atestado);
        }
        return $atestados;
    }

    public function buscar($id) {
        $sql = "select * from atestado where id = $id";
        $retorno = pg_query($this->conexao, $sql);
        return pg_fetch_array($retorno);
    }

}


