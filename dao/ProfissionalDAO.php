<?php

require_once 'dao/Conexao.php';

class ProfissionalDAO {

    private $conexao;
    private $usuarioDAO;

    public function __construct() {
        $this->conexao = Conexao::conectar();
        $this->usuarioDAO = new UsuarioDAO();
    }

    public function excluir($id) {
        $sql = "delete from profissional where id = $id";
        pg_query($this->conexao, $sql);
    }

    public function inserir(Pessoa $pessoa, Profissional $profissional) {

        $sql2 = "insert into endereco (rua, numero, cep) values "
            . "('{$pessoa->getEndereco()->getNumero()}', {$pessoa->getEndereco()->getRua()}, '{$pessoa->getEndereco()->getCep()}') returning id";

        $retorno_endereco = pg_query($this->conexao, $sql2);

        $endereco_id = pg_fetch_array($retorno_endereco);


        $sql1 = "insert into pessoa (nome, data_nasc, rg, cpf, sexo_id, endereco_id, telefoneprincipal, telefonesecundario) values "
            . "('{$pessoa->getNome()}', '{$pessoa->getDataNascimento()}', '{$pessoa->getRg()}', '{$pessoa->getCpf()}', {$pessoa->getSexo()}, 
            {$endereco_id[0]}, '{$pessoa->getTelefonePrincipal()}', '{$pessoa->getTelefoneSecundario()}') returning id";

        $retorno_pessoa = pg_query($this->conexao, $sql1);

        $pessoa_id = pg_fetch_array($retorno_pessoa);

        $this->usuarioDAO->inserir($pessoa->getUsuario()->getUsername(), $pessoa->getUsuario()->getSenha(), $pessoa_id['id']);

        $sql3 = "insert into profissional (tipo, pessoa_id) values "
            . "('{$profissional->getTipo()}', {$pessoa_id[0]}) returning id";

        pg_query($this->conexao, $sql3);

       /*
        $sql1 = "insert into profissional (tipo,"
                . "pessoa_id) values "
                . "('{$profissional->getTipo()}',"
                . "{$profissional->getPessoa()}) returning id";


        $retorno_profissional = pg_query($this->conexao, $sql1);

        $profissional_id = pg_fetch_array($retorno_profissional);
        */
    }

    public function listar() {
        $profissionais = array();
        $sql = "select profissional.*, pessoa.nome from profissional "
                . "  inner join pessoa on profissional.pessoa_id = pessoa.id ";
        $retorno = pg_query($this->conexao, $sql);
        while ($profissional = pg_fetch_array($retorno)) {
            array_push($profissionais, $profissional);
        }
        return $profissionais;
    }

    public function alterar(Profissional $profissional) {
        $sql = "update profissional set tipo = '{$profissional->getTipo()}', "
                . " pessoa_id = '{$profissional->getPessoa()}'"
                . " where id =  {$profissional->getId()}";
        pg_query($this->conexao, $sql);
    }

    public function buscar($id) {
        $sql = "select profissional.*, pessoa.nome from profissional"
                . " inner join pessoa on pessoa.id = profissional.pessoa_id"
                . " where profissional.id = $id";
        $retorno = pg_query($this->conexao, $sql);
        return pg_fetch_array($retorno);
    }

    public function getProfissional() {
        $sql = "select profissional.id from profissional inner join pessoa on pessoa.id = profissional.pessoa_id
              join usuario on pessoa.id = usuario.pessoa_id where usuario.username = '$_SESSION[username]'";
        $retorno = pg_query($this->conexao, $sql);

        $x = pg_fetch_array($retorno);
        return $x[0];
    }
}
