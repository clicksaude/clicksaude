<?php

include_once 'dao/Conexao.php';

class SexoDAO
{
    private $conexao;

    public function __construct() {
        $this->conexao = Conexao::conectar();
    }

    public function inserir(Sexo $sexo) {
        $sql = "insert into sexo (descricao, sigla) values ('{$sexo->getDescricao()}', '{$sexo->getSigla()}' )";
        pg_query($this->conexao, $sql);
    }

    public function excluir($id) {
        $sql = "delete from sexo where id = $id";
        pg_query($this->conexao, $sql);
    }

    public function alterar(Sexo $sexo) {
        $sql = "update sexo set descricao = '{$sexo->getDescricao()}', sigla = '{$sexo->getSigla()}' "
            . " where id = {$sexo->getId()}";
        pg_query($this->conexao, $sql);
    }

    public function listar() {
        $sexos = array();
        $sql = "select * from sexo order by descricao";
        $retorno = pg_query($this->conexao, $sql);
        while ($sexo = pg_fetch_array($retorno)) {
            array_push($sexos, $sexo);
        }
        return $sexos;
    }

    public function buscar($id) {
        $sql = "select * from sexo where id = $id";
        $retorno = pg_query($this->conexao, $sql);
        return pg_fetch_array($retorno);
    }
}