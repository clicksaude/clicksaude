<?php

include_once 'dao/Conexao.php';

class PaisDAO
{
    private $conexao;

    public function __construct() {
        $this->conexao = Conexao::conectar();
    }

    public function inserir(Pais $pais) {
        $sql = "insert into pais (nome, nacionalidade) values ('{$pais->getNome()}', '{$pais->getNacionalidade()}' )";
        pg_query($this->conexao, $sql);
    }

    public function excluir($id) {
        $sql = "delete from pais where id = $id";
        pg_query($this->conexao, $sql);
    }

    public function alterar(Pais $pais) {
        $sql = "update pais set nome = '{$pais->getNome()}', nacionalidade = '{$pais->getNacionalidade()}' "
            . " where id = {$pais->getId()}";
        pg_query($this->conexao, $sql);
    }

    public function listar() {
        $paises = array();
        $sql = "select * from pais order by nome";
        $retorno = pg_query($this->conexao, $sql);
        while ($pais = pg_fetch_array($retorno)) {
            array_push($paises, $pais);
        }
        return $paises;
    }

    public function buscar($id) {
        $sql = "select * from pais where id = $id";
        $retorno = pg_query($this->conexao, $sql);
        return pg_fetch_array($retorno);
    }
}